function Peci (str, arr) {

  this.outputTpl = '';

  this.parseSingle = function (str, obj) {
    this.res = str;
    var posStart = this.res.indexOf('{{');
    var posEnd = this.res.indexOf('}}');
    var positions = []; var count = 0;
    this.toChange = [];

    while (posStart != -1) {
      positions.push([posStart + 2, posEnd]);
      posStart = this.res.indexOf('{{', posStart + 3);
      posEnd = this.res.indexOf('}}', posEnd + 3);
      this.toChange.push(this.res.substring(positions[count][0], positions[count][1]));
      count++;
    }

    for (var i = 0; i < positions.length; i++) {
      this.res = this.res.replace(this.toChange[i], obj[this.toChange[i]]);
    }

    this.res = this.res.replace(/\{\{/g, '');
    this.res = this.res.replace(/\}\}/g, '')


    return this.res;
  };

  this.complete = function (str, arr) {
    for (var i = 0; i < arr.length; i++) {
      this.outputTpl += this.parseSingle(str, arr[i]);
    }
  }

  this.render = function () {
    this.complete(str, arr);
  }

  this.getTpl = function () {
    return this.outputTpl;
  }

};