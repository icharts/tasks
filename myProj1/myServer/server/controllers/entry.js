var _ = require('underscore');
var Uuid = require('node-uuid');
var fs = require('fs');

var dir = './data/';

function myError (code, msg) {
    return {code: code, msg: msg};
}

function getAllEntries (cb) {
    var entries = [];

    fs.readdir(dir, function (err, files) {
        if (err) {
            return cb(myError(401, 'Failed read directory with entries'));
        }

        _.each(files, function (v) {
            var obj = fs.readFileSync('./data/' + v);
            entries.push(JSON.parse(obj.toString()));
        });

        cb(null, entries);
    });
}

exports.getAllEntriesApi = function getAllEntriesApi (req, res) {
    getAllEntries(function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.header('Content-Type', 'application/json');
            res.send(JSON.stringify(data));
        }
    });
}

function getEntryById (EntryId, cb) {
    fs.readFile(dir + EntryId + '.json', function (err, data) {
        if (err) {
            return cb(myError(401, 'Entry with id '+ EntryId + ' not exist'));
        }

        cb(null, data.toString());
    });
}

exports.getEntryByIdApi = function getEntryByIdApi (req, res) {
    getEntryById(req.params.EntryId, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.header('Content-Type', 'application/json');
            res.send(data);
        }
    });
}

function addEntry (body, cb) {
    if (!body.Username) {
        return cb(myError(409, 'Username is mandatory'));
    }
    if (!body.Age) {
        return cb(myError(409, 'Age is mandatory'));
    }
    if (!body.Position) {
        return cb(myError(409, 'Position is mandatory'));
    }

    var EntryId = 'EntryId_' + Uuid.v4();

    var Entry = {
        EntryId: EntryId,
        Username: body.Username,
        Age: body.Age,
        Position: body.Position,
        DateCreated: Date.now(),
        DateModified: Date.now()
    };

    Entry = JSON.stringify(Entry);

    fs.writeFile(dir + EntryId + '.json', Entry, function (err) {
        if (err) {
            return cb(myError(401, 'Failed to add entry'))
        }

        cb(null, EntryId);
    });
}

exports.addEntryApi = function addEntryApi (req, res) {
    addEntry(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data)
        }
    });
}

function updateEntry (body, cb) {
    if (!body.EntryId) {
        return cb(myError(409, 'EntryId is mandatory'));
    }
    if (!body.Username) {
        return cb(myError(409, 'Username is mandatory'));
    }
    if (!body.Age) {
        return cb(myError(409, 'Age is mandatory'));
    }
    if (!body.Position) {
        return cb(myError(409, 'Position is mandatory'));
    }

    var Entry = {};

    getEntryById(body.EntryId, function (err, data) {
        if (err) {
            return cb(err);
        }

        Entry = JSON.parse(data);

        Entry.Username = body.Username;
        Entry.Age = body.Age;
        Entry.Position = body.Position;
        Entry.DateModified =  Date.now();

        Entry = JSON.stringify(Entry);

        fs.writeFile(dir + body.EntryId + '.json', Entry, function (err) {
            if (err) {
                return cb(myError(401, 'Failed to update entry'))
            }

            cb(null, 'Entry ' + body.Age + ' was successfully updated');
        });
    });

}

exports.updateEntryApi = function updateEntryApi (req, res) {
    updateEntry(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data)
        }
    });  
}

function deleteEntry (body, cb) {
    if (!body.EntryId) {
        return cb(myError(409, 'EntryId is mandatory'));
    }

    fs.unlink(dir + body.EntryId + '.json', function(err) {
        if (err) {
            return cb(myError(401, 'Failed to delete entry'));
        }

        cb(null, 'Entry with id ' + body.EntryId + ' was successfully deleted');
    });
}

exports.deleteEntryApi = function deleteEntryApi (req, res) {
    deleteEntry(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data);
        }
    });
}
