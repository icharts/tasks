var _ = require('underscore');
var Uuid = require('node-uuid');
var fs = require('fs');

var dir = './posts/';

function myError (code, msg) {
    return {code: code, msg: msg};
}

function getAllPosts (cb) {
    var posts = [];

    fs.readdir(dir, function (err, files) {
        if (err) {
            return cb(myError(401, 'Failed read directory with posts'));
        }

        _.each(files, function (v) {
            var obj = fs.readFileSync('./posts/' + v);
            posts.push(JSON.parse(obj.toString()));
        });

        cb(null, posts);
    });
}

exports.getAllPostsApi = function getAllPostsApi (req, res) {
    getAllPosts(function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.header('Content-Type', 'application/json');
            res.send(JSON.stringify(data));
        }
    });
}

function getPostById (PostId, cb) {
    fs.readFile(dir + PostId + '.json', function (err, data) {
        if (err) {
            return cb(myError(401, 'Post with id '+ PostId + ' not exist'));
        }

        cb(null, data.toString());
    });
}

exports.getPostByIdApi = function getPostByIdApi (req, res) {
    getPostById(req.params.PostId, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.header('Content-Type', 'application/json');
            res.send(data);
        }
    });
}

function addPost (body, cb) {
    if (!body.Title) {
        return cb(myError(409, 'Title is mandatory'));
    }
    if (!body.Category) {
        return cb(myError(409, 'Category is mandatory'));
    }
    if (!body.Text) {
        return cb(myError(409, 'Text is mandatory'));
    }

    var PostId = 'PostId_' + Uuid.v4();

    var Post = {
        PostId: PostId,
        Title: body.Title,
        Category: body.Category,
        Image: body.Image,
		Text: body.Text,
        DateCreated: Date.now(),
        DateModified: Date.now()
    };

    Post = JSON.stringify(Post);

    fs.writeFile(dir + PostId + '.json', Post, function (err) {
        if (err) {
            return cb(myError(401, 'Failed to add post'))
        }

        cb(null, PostId);
    });
}

exports.addPostApi = function addPostApi (req, res) {
    addPost(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data)
        }
    });
}

function updatePost (body, cb) {
    if (!body.PostId) {
        return cb(myError(409, 'PostId is mandatory'));
    }
   	if (!body.Title) {
        return cb(myError(409, 'Title is mandatory'));
    }
    if (!body.Category) {
        return cb(myError(409, 'Category is mandatory'));
    }
    if (!body.Text) {
        return cb(myError(409, 'Text is mandatory'));
    }

    var Post = {};

    getPostById(body.PostId, function (err, data) {
        if (err) {
            return cb(err);
        }

        Post = JSON.parse(data);

        Post.Title = body.Title,
        Post.Category = body.Category,
        Post.Image = body.Image,
		Post.Text = body.Text,
        Post.DateModified =  Date.now();

        Post = JSON.stringify(Post);

        fs.writeFile(dir + body.PostId + '.json', Post, function (err) {
            if (err) {
                return cb(myError(401, 'Failed to update post'))
            }

            cb(null, 'Post ' + body.Title + ' was successfully updated');
        });
    });

}

exports.updatePostApi = function updatePostApi (req, res) {
    updatePost(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data)
        }
    });  
}

function deletePost (body, cb) {
    if (!body.PostId) {
        return cb(myError(409, 'PostId is mandatory'));
    }

    fs.unlink(dir + body.PostId + '.json', function(err) {
        if (err) {
            return cb(myError(401, 'Failed to delete post'));
        }

        cb(null, 'Post with id ' + body.PostId + ' and title' + body.Post.Title + ' was successfully deleted');
    });
}

exports.deletePostApi = function deletePostApi (req, res) {
    deletePost(req.body, function (err, data) {
        if (err) {
            res.status(err.code).send(err.msg);
        } else {
            res.send(data);
        }
    });
}
