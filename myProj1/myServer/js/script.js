

function getEntryById (id) {
  var res = api.get(id);

  return JSON.parse(res);
}

function getAllEntries () {
  var res = api.get();

  return JSON.parse(res);
}

function addEntry (obj) {
  var res = JSON.stringify(obj);

  res = api.post("POST", res);
  return res;

}

function deleteEntry (id) {
  var res = JSON.stringify(id);
  res = api.post("DELETE", res);

  return res;
}

function updateEntry (update) {
  var res = JSON.stringify(update)
  res = api.post("PUT", res);

  return res;
}



var api = {};



api.post = function(method, body) {
  var xhr = new XMLHttpRequest();

  xhr.open(method, 'http://localhost:3333/entry/', false);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(body);

  return xhr.responseText;

}


api.get = function (route) {
  var xhr = new XMLHttpRequest();

  if (route) {
    xhr.open('GET', 'http://localhost:3333/entry/' + route, false);
  } else {
    xhr.open('GET', 'http://localhost:3333/entry/', false);
  }
  xhr.send();

  return xhr.responseText;
}


var update = {
    "Author":"Batman",
    "Title": "THE DARK KNIGHT",
    "Text": "updated",
    "EntryId": "EntryId_189fc7c3-68d1-478b-82c1-ee19e31ccc90"
};

var add = {
    "Author":"alamora",
    "Title": "kek",
    "Text": "azazaz keke"
};

var deleteE = {
    "EntryId": "EntryId_189fc7c3-68d1-478b-82c1-ee19e31ccc90"
};


console.log(getEntryById('EntryId_3a096cd1-753e-40c5-b812-4e34a112591b'));
console.log(getAllEntries());
//console.log(addEntry(add));
//console.log(deleteEntry(deleteE));
//console.log(updateEntry(update));
