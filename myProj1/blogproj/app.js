'use strict';

angular.module('webApp', ['ui.router', 'ngMaterial', 'ngAnimate', 'ngMaterialDatePicker', 'ngFileUpload']);
var serverLink = 'http://localhost:3333/';

(function () {
  'use strict';
  angular
    .module('webApp', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/');
      $stateProvider
        .state('home', {
          url: '/',
          // template: 'aaa',
          templateUrl: 'app/src/views/home.html',
          controller: 'homeController',
          controllerAs: 'homeCtrl'

        })
        .state('usersAll', {
          url: '/usersAll',
          templateUrl: 'app/src/views/userslist.html',
          // template: 'src/views/userslist.html'
          controller: 'usersAllCtrl',
          controllerAs: 'usersAll'

        })
        .state('addUser', {
          url: '/addUser',
          templateUrl: 'app/src/views/adduser.html',
          controller: 'addUserCtrl',
          controllerAs: 'addUser'
        })
        .state('addPost', {
          url: '/addPost',
          templateUrl: 'app/src/views/addpost.html',
          controller: 'addPostCtrl',
          controllerAs: 'addPost'
        })
        .state('signlePost', {
          url: '/post/:PostId',
          templateUrl: 'app/src/views/postsingle.html',
          controller: 'singlePostController',
          controllerAs: 'single'
        })
        .state('postUpdate', {
          url: '/postUpdate/:PostId',
          templateUrl: '/app/src/views/updatepost.html',
          controller: 'updatePostCtrl',
          controllerAs: 'updatePost'
        })
    }]);

}());
