(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('updatePostCtrl', function ($scope, $http, $stateParams) {
        var self = this;
        console.log($stateParams);
        self.data = 'a';
        $http.get(serverLink + 'post/' + $stateParams.PostId).then(function (response) {
          self.postData = response.data;
          console.log(self.postData);
        });

        self.updatePost = function updatePost (data) {
          $http({
            method: 'put',
            url: serverLink + 'post',
            data: data,
            headers: {
              'Content-Type': 'application/json'
            }
          }).then(function (response) {
            console.log(response.data);
          }, function (response) {
            console.log(response.data);
          });
        }

      });

}());
