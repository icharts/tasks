(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('usersAllCtrl', function ($scope, $http) {
        var self = this;

        $http.get(serverLink + 'entry').then(function (response) {
          console.log(response.data);
          self.userList = response.data;
        });

        self.deleteUser = function deleteUser (user) {
          document.querySelector( 'li[data-id="' + user.EntryId + '"]' ).remove();
          $http({
            method: 'delete',
            url: serverLink + 'entry',
            data: {'EntryId': user.EntryId},
            headers: {
              'Content-Type': 'application/json'
            }
          }
          ).then(function (response) {
            console.log(response.data);
          }, function (response) {
            console.log(response.data);
          })
        };

      });

}());
