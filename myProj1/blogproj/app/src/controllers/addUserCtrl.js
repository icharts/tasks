(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('addUserCtrl', function ($scope, $http) {
        var self = this;

        self.userData = {};

        self.addUser = function addUser () {
          self.sendData = JSON.stringify(self.userData);
          $http.post(serverLink + 'entry', self.sendData).then(function (response) {
            console.log(response.data);
            // var inputs = document.querySelectorAll('input[type="text"]');
            // inputs.forEach(function (elem) {
            //   elem.value = '';
            // });
            for (var key in self.userData) {
              self.userData[key] = '';
            }
          }, function (response) {
            console.log(response.data);
          })
        }

      });

}());
