(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('addPostCtrl', function ($scope, $http) {
        var self = this;

        self.postData = {};

        self.addPost = function addPost () {
          self.sendData = JSON.stringify(self.postData);
          $http.post(serverLink + 'post', self.sendData).then(function (response) {
            console.log(response.data);
            // var inputs = document.querySelectorAll('input[type="text"]');
            // inputs.forEach(function (elem) {
            //   elem.value = '';
            // });
            for (var key in self.postData) {
              self.postData[key] = '';
            }
          }, function (response) {
            console.log(response.data);
          })
        }

      });

}());
