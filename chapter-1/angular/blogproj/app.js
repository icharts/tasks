'use strict';

angular.module('webApp', ['ui.router', 'ngMaterial', 'ngAnimate', 'ngMaterialDatePicker', 'ngFileUpload']);

(function () {
  'use strict';
  angular
    .module('webApp', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/');
      $stateProvider
        .state('main', {
          url: '/',
          // template: 'aaa',
          templateUrl: 'src/views/main.html',
          controller: 'mainController'

        })
        .state('toDoList', {
          url: '/todolist',
          // templateUrl: 'src/views/todolist.html',
          template: 'todo'
        })
    }]);

}());
