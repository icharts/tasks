(function () {
  'use strict';

  angular
    .module('webApp')
    .controller('ModalCtrl', ['$scope', '$state', 'data', 'moment',  ModalCtrl]);

    function ModalCtrl ($scope, $state, data, moment) {
      $state.go('showall');
      $scope.moment = moment;
      $scope.data = data;
    }
    
}());
