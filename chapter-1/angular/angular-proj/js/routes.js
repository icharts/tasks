(function () {
  'use strict';

  angular
    .module('webApp', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
      $urlRouterProvider.otherwise('/show-all');
      $stateProvider
        .state('home', {
          url: '/',
          controller: 'ModalCtrl'
        })
        .state('showall', {
          url: '/show-all',
          templateUrl : 'tpls/show-all.html',
          controller: 'showAllController',
          controllerAs: 'showAll'
        })
        .state('showSignle', {
          url: '/show-single',
          templateUrl: 'tpls/show-single.html',
          controller: 'showSingleController'
        })
        .state('addEntry', {
          url: '/add-entry',
          templateUrl: 'tpls/add.html',
          controller: 'addEntryController'
        })
        .state('showOne', {
          url: '/show-all/:EntryId',
          templateUrl: 'tpls/show.detail.html',
          controller: 'showDetailsController'
        })
        .state('update', {
          url: '/update/:EntryId',
          templateUrl: 'tpls/update.html',
          controller: 'updateController'
        })
    })

    .controller('showAllController', function ($scope, $http, $compile, $filter) {
      var self = this;
      //$scope.myservice = myservice;

      $scope.setId = function (id) {
        $scope.myservise = id;
        console.log(id);
      }

      console.log(self.users);

      $http.get('http://localhost:3333/entry').then(function (response) {
        self.users = response.data;
        // $scope.users = $filter('orderBy')(response.data, 'DateCreated');
        console.log(response.data);

      });

      $scope.deleteEntry = function (user) {
        console.log(user.EntryId);

        $http({
          method: 'delete',
          url: 'http://localhost:3333/entry',
          data: {'EntryId': user.EntryId},
          headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            //console.log(user.EntryId);
            var Response = angular.element('<pre>' + response.data + '</pre>');
            // var target = angular.element( document.querySelector('#' + user.EntryId));
            var target = document.querySelector('#' + user.EntryId);
            console.log(Response);
            target.innerHTML = '<pre>' + response.data + '</pre>';
            //target.append(Response);
            $compile(Response)($scope);
        })
      }

      //console.log(self.users);
    })

    .controller('addEntryController', function ($scope, $http, $compile) {

        $scope.entry = {};

        $scope.addEntry = function addEntry (user) {
          $scope.entry = angular.copy(user);
          //console.log($scope.entry);
          //$http.post('https://localhost:3333/entry', $scope.entry);
          $http({
            method: 'post',
            url: 'http://localhost:3333/entry',
            data: $scope.entry,
            headers: {'Content-Type': 'application/json'}
          }).then(function (response) {
            //console.log(response.data);
            var Response = angular.element('<pre>' + response.data + '</pre>');
            var target = angular.element( document.querySelector('#wrapper'));
            target.append(Response);
            $compile(Response)($scope);
          });

        }

    })

    .controller('showDetailsController', function ($scope, $http,  $stateParams) {
      //$scope.user = {Author: 'aaa'};
      // console.log($scope);
      //$scope.myservice = myservice;
      console.log($stateParams);
      $scope.EntryId = $stateParams.EntryId;
      //console.log($scope.EntryId);

      $http.get('http://localhost:3333/entry/' + $scope.EntryId).then(function (response) {
        $scope.user = response.data;
        //console.log(response);
      });
    })

    .controller('updateController', function ($scope, $stateParams, $http, $compile) {
      // console.log($stateParams);

      $scope.EntryId = $stateParams.EntryId;

      $http.get('http://localhost:3333/entry/' + $scope.EntryId).then(function (response) {
        $scope.user = response.data;
      })

      $scope.entry = {};
      $scope.entry.EntryId = $scope.EntryId;

      $scope.updateEntry = function (user) {
        // $scope.entry = angular.copy(user);
        $http({
          method: 'put',
          url: 'http://localhost:3333/entry',
          data: $scope.user,
          headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            var Response = angular.element('<pre>' + response.data + '</pre>');
            var target = angular.element( document.querySelector('#wrapper'));
            target.append(Response);
            $compile(Response)($scope);
        })
      }


    })
}());
