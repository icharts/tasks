// example of object:
var obj = [
    {
        name: 'moisei',
        text: 'blabla'
    },
    {
        name: 'luka',
        text: 'alo'
    },
    {
        name: 'arhip',
        text: 'iuh'
    },
];

getValues(obj, 'text')
> ['blabla', 'alo', 'iuh']

getValues(obj, 'name')
> ['moisei', 'luka', 'arhip']

