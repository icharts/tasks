(function () {
  'use strict';
  function ironMan () {

    this.res = [];

    this.regEv = function (title, func) {
      this.res.push({name: title, action: func});
    };

    this.trigger = function (title, context) {

      var found = _.where(this.res, {name: title});

      for (var i = 0; i < found.length; i++) {
        found[i].action(context);
      }

    };

    this.delEv = function (title) {

      for (var i = 0; i < this.res.length; i++) {

        if (this.res[i].name === title) {
          this.res.splice(i, 1);
          i--;
        }

      }
    };

  };

  window.EventManager = ironMan;

})();




// var obj = new ironMan();

// obj.regEv('Shoot', function () { console.log('a shoot from first')});
// obj.regEv('Shoot', function () { console.log('a shoot from second')});
// obj.regEv('Shoot', function () { console.log('a shoot from another')});

// obj.regEv('fly', function () { console.log('i believe i can fly!')});
// obj.regEv('fly', function () { console.log('i believe i can touch the sky!')});
// obj.regEv('fly', function () { console.log('yea yea ya!')})

// obj.delEv('Shoot');

// obj.trigger('Shoot');
// console.log("\n")
// obj.trigger('fly');

//console.log(obj.res);
