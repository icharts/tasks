;(function () {
	var obj = {};

	obj.get = function get (url, success) {
		var xhr = new XMLHttpRequest();

		xhr.open('get', url, true);
		xhr.send();
		xhr.onreadystatechange = function () {
			if (xhr.readyState != 4) return;
			success(xhr.response);	
			//console.log(xhr.response);
		}
	}

	window.ajax = obj;

}());

