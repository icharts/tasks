;(function () {
	'use strict';
	var obj = {};

	obj.get = function get (url, success) {
		var xhr = new XMLHttpRequest();

		xhr.open('get', url, true);
		xhr.send();
		xhr.onreadystatechange = function () {
			if (xhr.readyState != 4) return;
			success(xhr.response);	
			//console.log(xhr.response);
		}
	}

	obj.post = function post (url, body, success) {
		var xhr = new XMLHttpRequest();
		
		xhr.open('post', url, true);
		xhr.setRequestHeader('Content-Type', 'application/json');

		xhr.send(body);
		xhr.onreadystatechange = function () {
			if (xhr.readyState != 4) return;
			success(xhr.response);
		}


	}

	obj.update = function update (url, body, success) {
		var xhr = new XMLHttpRequest();

		xhr.open('put', url, true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(body);
		xhr.onreadystatechange = function () {
			if (xhr.readyState != 4) return;
			success(xhr.response);
		}
	}

	obj.deleteEn = function deleteEn (url, body, success) {
		var xhr = new XMLHttpRequest();

		xhr.open('delete', url, true);
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.send(body);
		xhr.onreadystatechange = function () {
			if (xhr.readyState != 4) return;
			success(xhr.response);
		}
	}

	window.ajax = obj;

}());

