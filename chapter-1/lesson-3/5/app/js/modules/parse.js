;
(function () {
    'use strict';

    function TemplateEngine (tpl, collection) {
        var self = this;

        this.tpl = tpl;
        this.collection = collection;

        this.resultDoc = '';

        this.render = function render () {
            this.startParse();
        }

        this.getTemplate = function getTemplate () {
            return this.resultDoc;
        }

        this.parseOnce = function parseOnce (object) {
            /**
             * braces position
             * braces[0] - at start; braces[1] - at end
             * @type {Array}
             */
            var braces = [0, 0];

            var keyName = '';
            var result = '';

            var tpl = this.tpl;

            while (tpl.length > 0) {
                braces[0] = tpl.indexOf('{{'); braces[1] = tpl.indexOf('}}');

                if (!(braces[0] + 1)) {
                    result += tpl;
                    break;
                }

                keyName = tpl.slice(braces[0] + 2, braces[1]).trim();

                if (!object[keyName]) {
                    console.error('net takogo klyucha, sosi jopu');
                    return;
                }

                result += tpl.slice(0, braces[0]) + object[keyName];

                tpl = tpl.slice(braces[1] + 2);

            }

            return result;
        }

        this.startParse = function startParse () {
            this.collection.forEach(function (v) {
                self.resultDoc += self.parseOnce(v);
            });
        }
    }

    window.TemplateEngine = TemplateEngine;

}());
/*
;
(function Peci (str, arr) {
  'use strict';
  this.outputTpl = '';

  this.parseSingle = function (str, obj) {
    this.res = str;
    var posStart = this.res.indexOf('{{');
    var posEnd = this.res.indexOf('}}');
    var positions = []; var count = 0;
    this.toChange = [];

    while (posStart != -1) {
      positions.push([posStart + 2, posEnd]);
      posStart = this.res.indexOf('{{', posStart + 3);
      posEnd = this.res.indexOf('}}', posEnd + 3);
      this.toChange.push(this.res.substring(positions[count][0], positions[count][1]));
      count++;
    }

    for (var i = 0; i < positions.length; i++) {
      this.res = this.res.replace(this.toChange[i], obj[this.toChange[i]]);
    }

    this.res = this.res.replace(/\{\{/g, '');
    this.res = this.res.replace(/\}\}/g, '')


    return this.res;
  };

  this.complete = function (str, arr) {
    for (var i = 0; i < arr.length; i++) {
      this.outputTpl += this.parseSingle(str, arr[i]);
    }
  }

  this.render = function () {
    this.complete(str, arr);
  }

  this.getTpl = function () {
    return this.outputTpl;
  }
  
  window.TemplateEngine = Peci; 
}());*/
