'use strict';
        // task 10
function each(list, iterator) {

  if (Array.isArray(list)) {
    for (var i = 0; i < list.length; i++) {
      iterator(list[i], i, list);
    }
  } else {
    for (var key in list) {
      iterator(list[key], key, list);
    }
  }
}

function map(list, iterator) {

  var result = [];

  if (Array.isArray(list)) {
    for (var i = 0; i < list.length; i++) {
      result.push(iterator(list[i], i, list));
    }
  } else {
    for (var key in list) {
      result.push(iterator(list[key], key, list));
    }
  }

  return result;
}

function find(list, iterator) {
  if (Array.isArray(list)) {
    for (var i = 0; i < list.length; i++) {
      if (iterator(list[i])) return list[i];
    }
  } else {
    for (var key in list) {
      if (iterator(list[key])) return list[key];
    }
  }
}

function filter(list, iterator) {
  var result = [];

  if (Array.isArray(list)) {
    for (var i = 0; i < list.length; i++) {
      if (iterator(list[i])) result.push(list[i]);
    }
  } else {
    for (var key in list) {
      if (iterator(list[key])) result.push(list[key]);
    }
  }

  return result;
}

function where(list, properties) {
  var result = [];
  var flag;

  for (var i = 0; i < list.length; i++) {
    flag = true;

    for (var key in properties) {
      if (list[i][key] !== properties[key]) flag = false;
    }

    if (flag) result.push(list[i]);
  }

  return result;

}

function findWhere(list, properties) {
  var result = [];
  var flag;

  for (var i = 0; i < list.length; i++) {
    flag = true;

    for (var key in properties) {
      if (list[i][key] !== properties[key]) flag = false;
    }

    if (flag) {
      result.push(list[i]);
      return result;
    }
  }
}

function include(list, value) {

  if (Array.isArray(list)) {
    return (list.indexOf(value) === -1) ? false : true;
  } else {
    for (var key in list) {
      if (list[key] === value) return true;
    }

    return false;
  }
}

function pluck(arr, value) {
  var result = [];

  for (var i = 0; i < arr.length; i++)
    if (arr[i][value]) result.push(arr[i][value]);

  return result;
}

function values(obj) {
  var result = [];

  for (var key in obj) {
    result.push(obj[key]);
  }

  return result;
}

var stooges = [{name : 'moe', age : 40}, {age: 40, namee: 'Kot'}, {name : 'larry', age : 50}, {name : 'curly', age : 60}];
console.log(values({one : 1, two : 2, three : 3}));

var listOfPlays = [
  {
    title: "Cymbeline",
    author: "Shakespeare",
    year: 1611,
    book: true
  },
  {
    title: "The Tempest",
    author: "Shakespeare",
    year: 1611,
    book: true
  },
  {
    title: "Ruslan and Ludmila",
    author: "Pushkin",
    year: 1833,
    book: true
  }
];

