(function() {
  var obj = {};

  obj.without = function without(array) {
    var res = [];
    var flag = true;

    for (var i = 0; i < array.length; i++) {
      flag = true;
      for (var j = 0; j < arguments.length; j++) {
        if (array[i] === arguments[j]) flag = false;
      }
      if (flag) res.push(array[i]);
    }

    return res;
  }


  obj.each = function each(list, iterator) {

    if (Array.isArray(list)) {
      for (var i = 0; i < list.length; i++) {
        iterator(list[i], i, list);
      }
    } else {
      for (var key in list) {
        iterator(list[key], key, list);
      }
    }
  };

  obj.map = function map(list, iterator) {

    var result = [];

    if (Array.isArray(list)) {
      for (var i = 0; i < list.length; i++) {
        result.push(iterator(list[i], i, list));
      }
    } else {
      for (var key in list) {
        result.push(iterator(list[key], key, list));
      }
    }

    return result;
  };

  obj.find = function find(list, iterator) {
    if (Array.isArray(list)) {
      for (var i = 0; i < list.length; i++) {
        if (iterator(list[i])) return list[i];
      }
    } else {
      for (var key in list) {
        if (iterator(list[key])) return list[key];
      }
    }
  };

  obj.filter = function filter(list, iterator) {
    var result = [];

    if (Array.isArray(list)) {
      for (var i = 0; i < list.length; i++) {
        if (iterator(list[i])) result.push(list[i]);
      }
    } else {
      for (var key in list) {
        if (iterator(list[key])) result.push(list[key]);
      }
    }

    return result;
  }

  obj.where = function where(list, properties) {
    var result = [];
    var flag;

    for (var i = 0; i < list.length; i++) {
      flag = true;

      for (var key in properties) {
        if (list[i][key] !== properties[key]) flag = false;
      }

      if (flag) result.push(list[i]);
    }

    return result;

  };

  obj.findWhere = function findWhere(list, properties) {
    var result = [];
    var flag;

    for (var i = 0; i < list.length; i++) {
      flag = true;

      for (var key in properties) {
        if (list[i][key] !== properties[key]) flag = false;
      }

      if (flag) {
        result.push(list[i]);
        return result;
      }
    }
  };

  obj.contains = function contains(list, value) {

    if (Array.isArray(list)) {
      return (list.indexOf(value) === -1) ? false : true;
    } else {
      for (var key in list) {
        if (list[key] === value) return true;
      }

      return false;
    }
  }

  obj.pluck = function pluck(arr, value) {
    var result = [];

    for (var i = 0; i < arr.length; i++)
      if (arr[i][value]) result.push(arr[i][value]);

    return result;
  };

  obj.values = function values(obj) {
    var result = [];

    for (var key in obj) {
      result.push(obj[key]);
    }

    return result;
  };

  window._ = obj;
})();
