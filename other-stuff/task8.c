#include <stdio.h>
//#include <conio.h>
#include <stdlib.h>
#include <time.h>


int main() {

    int n, randomJ, randomI, count = 0;

    printf("Enter the triangle size: ");
    scanf("%d", &n);

    //generate random number for a star
    srand(time(NULL));


    if (n < 4) {
      randomJ = 0;
      randomI = 0;
    }
    else if (n == 4){
        randomJ = 1;
        randomI = 2;
    } else {
        randomI = 2 + rand() % (n - 3);
        randomJ = 1 + rand() % (randomI - 1);
        //if(randomJ == randomI) randomJ--;
    }


    if (randomJ && randomI)
      printf("\n Random number generator: j = %d i = %d\n", randomJ, randomI);
    else
      printf("\n The point doesn't exist. \n");

    printf("\n");

    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= i; j++) {
            if (n > 3 && i == randomI && j == randomJ) {
                printf("#");
            }
            else printf("*");
        }
        printf("\n");
    }





    //getch();
    return 0;
}

