#include <stdio.h>
#include <string.h>
//#include <conio.h>

// Converting numbers from decimal base to any other.

  int main() {

    int value;
    int numSys;
    int x;
    int y;
    int count = 0;
    char res[66] = {"*"};

    printf("%lu - ", strlen(res));

    printf("\n Enter a decimal number to convert: ");
    scanf("%d", &value);

    printf("\n Enter the numeral system to convert your number: ");
    scanf("%d", &numSys);

    printf("\n Answer is: ");

    if (numSys > 1 && numSys <= 16) {

      while (value) {
        x = (value % numSys);
        value = (value / numSys);

        res[count] = x;
        count++;
      }

      while (count) {
        switch (res[count - 1]) {
          case 10: printf("%s", "A"); break;
          case 11: printf("%s", "B"); break;
          case 12: printf("%s", "C"); break;
          case 13: printf("%s", "D"); break;
          case 14: printf("%s", "E"); break;
          case 15: printf("%s", "F"); break;


          default : printf("%d", res[count - 1]);
        }
        count--;
      }



    } else {
      printf("\n Your number is wrong. \n");
    }
    printf("\n");
    printf("\n ");

  return 0;
}

